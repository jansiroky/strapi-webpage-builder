export default  function getSvgHtml(svg) {
  const svgDiv = document.createElement("img");
  svgDiv.setAttribute('width', '100%');
  svgDiv.setAttribute('height', '100%');
  svgDiv.setAttribute('src', svg);
  return svgDiv.outerHTML;
}
