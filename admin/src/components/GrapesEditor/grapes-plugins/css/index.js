import config from "../../config"

const appendTailwindCss = (editor) => {
  const iframe = editor.Canvas.getFrameEl()

  if (!iframe) return;

  const cssLink = document.createElement('link');
  cssLink.href = config.tailwindCssUrl;
  cssLink.rel = 'stylesheet';
  iframe.contentDocument.head.appendChild(cssLink);

  const cssStyle = document.createElement('style');
  cssStyle.type = 'text/css';
  cssStyle.innerHTML = `img.object-cover { filter: sepia(1) hue-rotate(190deg) opacity(.46) grayscale(.7) !important; }`;
  iframe.contentDocument.head.appendChild(cssStyle);
}

const appendThemeCss = (editor) => {
  const iframe = editor.Canvas.getFrameEl();

  if (!iframe) return;

  const cssLink = document.createElement('link');
  cssLink.href = config.themeCssUrl;
  cssLink.rel = 'stylesheet';
  iframe.contentDocument.head.appendChild(cssLink);
}

/*const appendCustomCss = () => {
  document.querySelector('html').style.height = '100%'
  document.querySelector('body').style.height = '100%'
}*/

const appendCss = (editor) => {
  // appendCustomCss()
  appendTailwindCss(editor)
  appendThemeCss(editor);
}
export { appendCss }
