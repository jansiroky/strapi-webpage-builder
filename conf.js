require("dotenv").config();

module.exports = {
  env: {
    TAILWIND_CSS_URL: process.env.TAILWIND_CSS_URL,
    THEME_CSS_URL: process.env.THEME_CSS_URL
  }
}
