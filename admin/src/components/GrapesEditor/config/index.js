export default {
  tailwindCssUrl: "https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.4/tailwind.min.css",
  themeCssUrl: process.env.THEME_CSS_URL || "http://localhost:8080/theme"
}
