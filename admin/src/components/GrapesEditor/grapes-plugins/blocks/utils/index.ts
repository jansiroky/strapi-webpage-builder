export default  function getSvgHtml(svg: any) {
  const svgEl = svg();
  svgEl.setAttribute('width', '100%');
  svgEl.setAttribute('height', '100%');
  return svgEl.outerHTML;
}
