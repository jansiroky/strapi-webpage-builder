import { source as b1 } from './data/blog-1'
import b1s from './data/blog-1.svg'
import { source as b2 } from './data/blog-2'
import b2s from './data/blog-2.svg'
import { source as b3 } from './data/blog-3'
import b3s from './data/blog-3.svg'
import { source as b4 } from './data/blog-4'
import b4s from './data/blog-4.svg'
import { source as b5 } from './data/blog-5'
import b5s from './data/blog-5.svg'

import { source as c1 } from './data/contact-1'
import c1s from './data/contact-1.svg'
import { source as c2 } from './data/contact-2'
import c2s from './data/contact-2.svg'
import { source as c3 } from './data/contact-3'
import c3s from './data/contact-3.svg'

import { source as d1 } from './data/content-1'
import d1s from './data/content-1.svg'
import { source as d2 } from './data/content-2'
import d2s from './data/content-2.svg'
import { source as d3 } from './data/content-3'
import d3s from './data/content-3.svg'
import { source as d4 } from './data/content-4'
import d4s from './data/content-4.svg'
import { source as d5 } from './data/content-5'
import d5s from './data/content-5.svg'
import { source as d6 } from './data/content-6'
import d6s from './data/content-6.svg'
import { source as d7 } from './data/content-7'
import d7s from './data/content-7.svg'
import { source as d8 } from './data/content-8'
import d8s from './data/content-8.svg'

import { source as a1 } from './data/cta-1'
import a1s from './data/cta-1.svg'
import { source as a2 } from './data/cta-2'
import a2s from './data/cta-2.svg'
import { source as a3 } from './data/cta-3'
import a3s from './data/cta-3.svg'
import { source as a4 } from './data/cta-4'
import a4s from './data/cta-4.svg'

import { source as e1 } from './data/ecommerce-1'
import e1s from './data/ecommerce-1.svg'
import { source as e2 } from './data/ecommerce-2'
import e2s from './data/ecommerce-2.svg'
import { source as e3 } from './data/ecommerce-3'
import e3s from './data/ecommerce-3.svg'

import { source as f1 } from './data/feature-1'
import f1s from './data/feature-1.svg'
import { source as f2 } from './data/feature-2'
import f2s from './data/feature-2.svg'
import { source as f3 } from './data/feature-3'
import f3s from './data/feature-3.svg'
import { source as f4 } from './data/feature-4'
import f4s from './data/feature-4.svg'
import { source as f5 } from './data/feature-5'
import f5s from './data/feature-5.svg'
import { source as f6 } from './data/feature-6'
import f6s from './data/feature-6.svg'
import { source as f7 } from './data/feature-7'
import f7s from './data/feature-7.svg'
import { source as f8 } from './data/feature-8'
import f8s from './data/feature-8.svg'

import { source as z1 } from './data/footer-1'
import z1s from './data/footer-1.svg'
import { source as z2 } from './data/footer-2'
import z2s from './data/footer-2.svg'
import { source as z3 } from './data/footer-3'
import z3s from './data/footer-3.svg'
import { source as z4 } from './data/footer-4'
import z4s from './data/footer-4.svg'
import { source as z5 } from './data/footer-5'
import z5s from './data/footer-5.svg'

import { source as g1 } from './data/gallery-1'
import g1s from './data/gallery-1.svg'
import { source as g2 } from './data/gallery-2'
import g2s from './data/gallery-2.svg'
import { source as g3 } from './data/gallery-3'
import g3s from './data/gallery-3.svg'

import { source as h1 } from './data/header-1'
import h1s from './data/header-1.svg'
import { source as h2 } from './data/header-2'
import h2s from './data/header-2.svg'
import { source as h3 } from './data/header-3'
import h3s from './data/header-3.svg'
import { source as h4 } from './data/header-4'
import h4s from './data/header-4.svg'

import { source as r1 } from './data/hero-1'
import r1s from './data/hero-1.svg'
import { source as r2 } from './data/hero-2'
import r2s from './data/hero-2.svg'
import { source as r3 } from './data/hero-3'
import r3s from './data/hero-3.svg'
import { source as r4 } from './data/hero-4'
import r4s from './data/hero-4.svg'
import { source as r5 } from './data/hero-5'
import r5s from './data/hero-5.svg'
import { source as r6 } from './data/hero-6'
import r6s from './data/hero-6.svg'

import { source as p1 } from './data/pricing-1'
import p1s from './data/pricing-1.svg'
import { source as p2 } from './data/pricing-2'
import p2s from './data/pricing-2.svg'

import { source as s1 } from './data/statistic-1'
import s1s from './data/statistic-1.svg'
import { source as s2 } from './data/statistic-2'
import s2s from './data/statistic-2.svg'
import { source as s3 } from './data/statistic-3'
import s3s from './data/statistic-3.svg'

import { source as q1 } from './data/step-1'
import q1s from './data/step-1.svg'
import { source as q3 } from './data/step-3'
import q3s from './data/step-3.svg'

import { source as t1 } from './data/team-1'
import t1s from './data/team-1.svg'
import { source as t2 } from './data/team-2'
import t2s from './data/team-2.svg'
import { source as t3 } from './data/team-3'
import t3s from './data/team-3.svg'

import { source as m1 } from './data/testimonial-1'
import m1s from './data/testimonial-1.svg'
import { source as m2 } from './data/testimonial-2'
import m2s from './data/testimonial-2.svg'
import { source as m3 } from './data/testimonial-3'
import m3s from './data/testimonial-3.svg'


import getSvgHtml from "./utils/index";

// @ts-ignore
const sources = [
  {
    id: 'blog-block-1',
    class: '',
    label: getSvgHtml(b1s),
    content: b1,
    category: 'Blog',
  },
  {
    id: 'blog-block-2',
    class: '',
    label: getSvgHtml(b2s),
    content: b2,
    category: 'Blog',
  },
  {
    id: 'blog-block-3',
    class: '',
    label: getSvgHtml(b3s),
    content: b3,
    category: 'Blog',
  },
  {
    id: 'blog-block-4',
    class: '',
    label: getSvgHtml(b4s),
    content: b4,
    category: 'Blog',
  },
  {
    id: 'blog-block-5',
    class: '',
    label: getSvgHtml(b5s),
    content: b5,
    category: 'Blog',
  },
  {
    id: 'contact-block-1',
    class: '',
    label: getSvgHtml(c1s),
    content: c1,
    category: 'Contact',
  },
  {
    id: 'contact-block-2',
    class: '',
    label: getSvgHtml(c2s),
    content: c2,
    category: 'Contact',
  },
  {
    id: 'contact-block-3',
    class: '',
    label: getSvgHtml(c3s),
    content: c3,
    category: 'Contact',
  },
  {
    id: 'content-block-1',
    class: '',
    label: getSvgHtml(d1s),
    content: d1,
    category: 'Content',
  },
  {
    id: 'content-block-2',
    class: '',
    label: getSvgHtml(d2s),
    content: d2,
    category: 'Content',
  },
  {
    id: 'content-block-3',
    class: '',
    label: getSvgHtml(d3s),
    content: d3,
    category: 'Content',
  },
  {
    id: 'content-block-4',
    class: '',
    label: getSvgHtml(d4s),
    content: d4,
    category: 'Content',
  },
  {
    id: 'content-block-5',
    class: '',
    label: getSvgHtml(d5s),
    content: d5,
    category: 'Content',
  },
  {
    id: 'content-block-6',
    class: '',
    label: getSvgHtml(d6s),
    content: d6,
    category: 'Content',
  },
  {
    id: 'content-block-7',
    class: '',
    label: getSvgHtml(d7s),
    content: d7,
    category: 'Content',
  },
  {
    id: 'content-block-8',
    class: '',
    label: getSvgHtml(d8s),
    content: d8,
    category: 'Content',
  },
  {
    id: 'cta-block-1',
    class: '',
    label: getSvgHtml(a1s),
    content: a1,
    category: 'CTA',
  },
  {
    id: 'cta-block-2',
    class: '',
    label: getSvgHtml(a2s),
    content: a2,
    category: 'CTA',
  },
  {
    id: 'cta-block-3',
    class: '',
    label: getSvgHtml(a3s),
    content: a3,
    category: 'CTA',
  },
  {
    id: 'cta-block-4',
    class: '',
    label: getSvgHtml(a4s),
    content: a4,
    category: 'CTA',
  },
  {
    id: 'commerce-block-1',
    class: '',
    label: getSvgHtml(e1s),
    content: e1,
    category: 'Commerce',
  },
  {
    id: 'commerce-block-2',
    class: '',
    label: getSvgHtml(e2s),
    content: e2,
    category: 'Commerce',
  },
  {
    id: 'commerce-block-3',
    class: '',
    label: getSvgHtml(e3s),
    content: e3,
    category: 'Commerce',
  },
  {
    id: 'feature-block-1',
    class: '',
    label: getSvgHtml(f1s),
    content: f1,
    category: 'Features',
  },
  {
    id: 'feature-block-2',
    class: '',
    label: getSvgHtml(f2s),
    content: f2,
    category: 'Features',
  },
  {
    id: 'feature-block-3',
    class: '',
    label: getSvgHtml(f3s),
    content: f3,
    category: 'Features',
  },
  {
    id: 'feature-block-4',
    class: '',
    label: getSvgHtml(f4s),
    content: f4,
    category: 'Features',
  },
  {
    id: 'feature-block-5',
    class: '',
    label: getSvgHtml(f5s),
    content: f5,
    category: 'Features',
  },
  {
    id: 'feature-block-6',
    class: '',
    label: getSvgHtml(f6s),
    content: f6,
    category: 'Features',
  },
  {
    id: 'feature-block-7',
    class: '',
    label: getSvgHtml(f7s),
    content: f7,
    category: 'Features',
  },
  {
    id: 'feature-block-8',
    class: '',
    label: getSvgHtml(f8s),
    content: f8,
    category: 'Features',
  },
  {
    id: 'footer-block-1',
    class: '',
    label: getSvgHtml(z1s),
    content: z1,
    category: 'Footer',
  },
  {
    id: 'footer-block-2',
    class: '',
    label: getSvgHtml(z2s),
    content: z2,
    category: 'Footer',
  },
  {
    id: 'footer-block-3',
    class: '',
    label: getSvgHtml(z3s),
    content: z3,
    category: 'Footer',
  },
  {
    id: 'footer-block-4',
    class: '',
    label: getSvgHtml(z4s),
    content: z4,
    category: 'Footer',
  },
  {
    id: 'footer-block-5',
    class: '',
    label: getSvgHtml(z5s),
    content: z5,
    category: 'Footer',
  },
  {
    id: 'gallery-block-1',
    class: '',
    label: getSvgHtml(g1s),
    content: g1,
    category: 'Gallery',
  },
  {
    id: 'gallery-block-2',
    class: '',
    label: getSvgHtml(g2s),
    content: g2,
    category: 'Gallery',
  },
  {
    id: 'gallery-block-3',
    class: '',
    label: getSvgHtml(g3s),
    content: g3,
    category: 'Gallery',
  },
  {
    id: 'header-block-1',
    class: '',
    label: getSvgHtml(h1s),
    content: h1,
    category: 'Header',
  },
  {
    id: 'header-block-2',
    class: '',
    label: getSvgHtml(h2s),
    content: h2,
    category: 'Header',
  },
  {
    id: 'header-block-3',
    class: '',
    label: getSvgHtml(h3s),
    content: h3,
    category: 'Header',
  },
  {
    id: 'header-block-4',
    class: '',
    label: getSvgHtml(h4s),
    content: h4,
    category: 'Header',
  },
  {
    id: 'hero-block-1',
    class: '',
    label: getSvgHtml(r1s),
    content: r1,
    category: 'Hero',
  },
  {
    id: 'hero-block-2',
    class: '',
    label: getSvgHtml(r2s),
    content: r2,
    category: 'Hero',
  },
  {
    id: 'hero-block-3',
    class: '',
    label: getSvgHtml(r3s),
    content: r3,
    category: 'Hero',
  },
  {
    id: 'hero-block-4',
    class: '',
    label: getSvgHtml(r4s),
    content: r4,
    category: 'Hero',
  },
  {
    id: 'hero-block-5',
    class: '',
    label: getSvgHtml(r5s),
    content: r5,
    category: 'Hero',
  },
  {
    id: 'hero-block-6',
    class: '',
    label: getSvgHtml(r6s),
    content: r6,
    category: 'Hero',
  },
  {
    id: 'pricing-block-1',
    class: '',
    label: getSvgHtml(p1s),
    content: p1,
    category: 'Pricing',
  },
  {
    id: 'pricing-block-2',
    class: '',
    label: getSvgHtml(p2s),
    content: p2,
    category: 'Pricing',
  },
  {
    id: 'statistic-block-1',
    class: '',
    label: getSvgHtml(s1s),
    content: s1,
    category: 'Statistics',
  },
  {
    id: 'statistic-block-2',
    class: '',
    label: getSvgHtml(s2s),
    content: s2,
    category: 'Statistics',
  },
  {
    id: 'statistic-block-3',
    class: '',
    label: getSvgHtml(s3s),
    content: s3,
    category: 'Statistics',
  },
  {
    id: 'step-block-1',
    class: '',
    label: getSvgHtml(q1s),
    content: q1,
    category: 'Steps',
  },
  {
    id: 'step-block-3',
    class: '',
    label: getSvgHtml(q3s),
    content: q3,
    category: 'Steps',
  },
  {
    id: 'team-block-1',
    class: '',
    label: getSvgHtml(t1s),
    content: t1,
    category: 'Team',
  },
  {
    id: 'team-block-2',
    class: '',
    label: getSvgHtml(t2s),
    content: t2,
    category: 'Team',
  },
  {
    id: 'team-block-3',
    class: '',
    label: getSvgHtml(t3s),
    content: t3,
    category: 'Team',
  },
  {
    id: 'testimonial-block-1',
    class: '',
    label: getSvgHtml(m1s),
    content: m1,
    category: 'Testimonials',
  },
  {
    id: 'testimonial-block-2',
    class: '',
    label: getSvgHtml(m2s),
    content: m2,
    category: 'Testimonials',
  },
  {
    id: 'testimonial-block-3',
    class: '',
    label: getSvgHtml(m3s),
    content: m3,
    category: 'Testimonials',
  },
]


// @ts-ignore
export function loadTailwindBlocks(blockManager) {
  for (const source of sources) {
    blockManager.add(source.id, {
      label: source.label,
      attributes: { class: source.class },
      content: source.content,
      category: { label: source.category, open: source.category === 'Blog' },
    })
  }
}
